package com.ojeanahuel.dds.controller;

import com.ojeanahuel.dds.service.DocumentFactoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/documents")
public class DocumentController {

    private static final Logger log = LoggerFactory.getLogger(DocumentController.class);

    @Autowired private DocumentFactoryService documentFactoryService;

    @GetMapping(value = "/{DOCUMENT_TYPE}/{INSTRUMENT_ID}_{DOCUMENT_LANGUAGE}.pdf", produces = "application/pdf")
    public ResponseEntity<byte[]> getDocument(@PathVariable("DOCUMENT_TYPE") String docType,
                                                  @PathVariable("INSTRUMENT_ID") String instrumentId,
                                                  @PathVariable("DOCUMENT_LANGUAGE") String docLanguage){
        try {
            byte[] pdfFile = documentFactoryService.getDocument(docType, instrumentId, docLanguage);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType("application/pdf"));
            String fileName = instrumentId.concat("_").concat(docLanguage).concat(".pdf");
            headers.setContentDispositionFormData("inline", fileName);

            return new ResponseEntity<>(pdfFile, headers, HttpStatus.OK);
        } catch (Exception ex){
            log.error(ex.getMessage(), ex);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
