package com.ojeanahuel.dds.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MDPResponseDTO implements Serializable {

    @JsonProperty("documents")
    private DocumentResponseDTO documentResponseDTO;

    public DocumentResponseDTO getDocumentResponseDTO() {
        return documentResponseDTO;
    }

    public void setDocumentResponseDTO(DocumentResponseDTO documentResponseDTO) {
        this.documentResponseDTO = documentResponseDTO;
    }
}
