package com.ojeanahuel.dds.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DocumentResponseDTO implements Serializable {

    @JsonProperty("kiid")
    private KiidResponseDTO kiidResponseDTO;

    public KiidResponseDTO getKiidResponseDTO() {
        return kiidResponseDTO;
    }

    public void setKiidResponseDTO(KiidResponseDTO kiidResponseDTO) {
        this.kiidResponseDTO = kiidResponseDTO;
    }
}
