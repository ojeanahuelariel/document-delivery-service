package com.ojeanahuel.dds.datatype;

public enum EDocumentProvider {
    VM_DATEN, MARKET_DATA_PLATFORM, LOCAL_FILE;
}
