package com.ojeanahuel.dds.datatype;

public enum EDocumentLanguage {
    DE("DE"), EN("EN");

    private final String value;

    /**
     * @param value This key identifies the language
     * the document should be in. DE
     * means German and En means
     * English.
     */
    EDocumentLanguage(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static EDocumentLanguage getLanguage (String lang){
        switch (lang){
            case "DE":
                return DE;
            case "EN":
                return EN;
            default:
                return null;
        }
    }
}
