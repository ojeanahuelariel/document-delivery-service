package com.ojeanahuel.dds.datatype;

public enum EDocumentType {

    BIB ("bib", EDocumentProvider.VM_DATEN),
    PRIIP_PORTRAIT("priip-portrait", EDocumentProvider.VM_DATEN),
    PROSP_VPZ("prosp-vpz", EDocumentProvider.VM_DATEN),
    PIB("pib", EDocumentProvider.LOCAL_FILE),
    PRORTRAIT("prortrait", EDocumentProvider.LOCAL_FILE),
    KIID("kiid", EDocumentProvider.MARKET_DATA_PLATFORM),
    WA("wa", EDocumentProvider.MARKET_DATA_PLATFORM),
    PROSP_VP("prosp-vp", EDocumentProvider.MARKET_DATA_PLATFORM),
    PROSP_JB("prosp-jb", EDocumentProvider.MARKET_DATA_PLATFORM),
    PROSP_HJB("prosp-hjb", EDocumentProvider.MARKET_DATA_PLATFORM);

    private final String value;
    private final EDocumentProvider provider;

    /**
     * @param value This key refers to the Deutsche
     * Bank internal document
     * category. Please note that this
     * key is known only to Deutsche
     * Bank and WebFG. When
     * interacting with 3rd party
     * document providers, we must
     * map this key to the providers
     */
    EDocumentType(String value, EDocumentProvider provider) {
        this.value = value;
        this.provider = provider;
    }

    public EDocumentProvider getProvider() {
        return provider;
    }

    public static EDocumentType getDocumentType(String value){
        switch (value.toLowerCase()){
            case "bib":
                return BIB;
            case "priip-portrait":
                return PRIIP_PORTRAIT;
            case "prosp-vpz":
                return PROSP_VPZ;
            case "pib":
                return PIB;
            case "prortrait":
                return PRORTRAIT;
            case "kiid":
                return KIID;
            case "wa":
                return WA;
            case "prosp-vp":
                return PROSP_VP;
            case "prosp-jb":
                return PROSP_JB;
            case "prosp-hjb":
                return PROSP_HJB;
            default:
                return null;
        }
    }
}
