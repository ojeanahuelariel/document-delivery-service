package com.ojeanahuel.dds.client;

import com.ojeanahuel.dds.dto.MDPResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class MarketDataPlataformClient {

    @Value("${mdp-url}") protected String MDP_URL;

    private static final String ENDPOINT_URL = "/mdp-rest/fww-documents/";

    @Autowired private RestTemplate restTemplate;

    public String getDownloadUrl (String instrumentId) throws Exception {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            HttpEntity<?> entity = new HttpEntity<>(headers);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(MDP_URL.concat(ENDPOINT_URL).concat(instrumentId));

            ResponseEntity<MDPResponseDTO> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, MDPResponseDTO.class);

            MDPResponseDTO mdpResponseDTO = response.getBody();

            return mdpResponseDTO.getDocumentResponseDTO().getKiidResponseDTO().getDownloadUrl();
        }catch (HttpClientErrorException ex) {
            throw new Exception("Request error to MDP");
        }
    }
}
