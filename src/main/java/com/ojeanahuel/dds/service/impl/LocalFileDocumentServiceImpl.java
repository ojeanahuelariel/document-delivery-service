package com.ojeanahuel.dds.service.impl;

import com.ojeanahuel.dds.service.LocalFileDocumentService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class LocalFileDocumentServiceImpl implements LocalFileDocumentService {

    private static final String PATH_PDF = "\\data\\mdp-doc\\";

    private DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");

    @Value("${local-file-url}") protected String LOCAL_FILE;

    @Override
    public byte[] getDocument(String docType, String instrumentId, String docLanguage) {

        String fileName = docType.concat("_").concat(instrumentId)
                .concat("_").concat(docLanguage).concat("_")
                .concat(dateFormat.format(new Date())).concat(".pdf");

        Path path = Paths.get(LOCAL_FILE.concat(PATH_PDF).concat(fileName));
        byte[] pdfContents = null;
        try {
            pdfContents = Files.readAllBytes(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pdfContents;
    }
}
