package com.ojeanahuel.dds.service.impl;

import com.ojeanahuel.dds.client.MarketDataPlataformClient;
import com.ojeanahuel.dds.service.MarketDataPlataformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class MarketDataPlataformServiceImpl implements MarketDataPlataformService {

    @Autowired private MarketDataPlataformClient marketDataPlataformClient;

    @Override
    public byte[] getDocument(String docType, String instrumentId, String docLanguage) throws Exception {

        String downloadUrl = marketDataPlataformClient.getDownloadUrl(instrumentId);

        File file = new File(downloadUrl);
        return new byte[(int) file.length()];
    }
}
