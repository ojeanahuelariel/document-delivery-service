package com.ojeanahuel.dds.service.impl;

import com.ojeanahuel.dds.datatype.EDocumentLanguage;
import com.ojeanahuel.dds.datatype.EDocumentType;
import com.ojeanahuel.dds.service.DocumentFactoryService;
import com.ojeanahuel.dds.service.LocalFileDocumentService;
import com.ojeanahuel.dds.service.MarketDataPlataformService;
import com.ojeanahuel.dds.service.VMDatenDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DocumentFactoryServiceImpl implements DocumentFactoryService {

    @Autowired private VMDatenDocumentService vmDatenDocumentService;
    @Autowired private LocalFileDocumentService localFileDocumentService;
    @Autowired private MarketDataPlataformService marketDataPlataformService;

    @Override
    public byte[] getDocument(String docType, String instrumentId, String docLanguage) throws Exception {

        if (EDocumentLanguage.getLanguage(docLanguage) == null){
            throw new Exception("Language doesn't exist or is null");
        }

        if (instrumentId == null || instrumentId.isEmpty()){
            throw new Exception("Instrument_Id is empty or null");
        }

        EDocumentType documentType = EDocumentType.getDocumentType(docType);
        if (documentType == null){
            throw new Exception("Datatype doesn't exist or is null");
        }

        switch (documentType.getProvider()){
            case VM_DATEN:
                return vmDatenDocumentService.getDocument(docType, instrumentId, docLanguage);
            case LOCAL_FILE:
                return localFileDocumentService.getDocument(docType, instrumentId, docLanguage);
            case MARKET_DATA_PLATFORM:
                return marketDataPlataformService.getDocument(docType, instrumentId, docLanguage);
            default:
                throw new Exception("Error to get document");
        }
    }
}
