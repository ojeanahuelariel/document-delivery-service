package com.ojeanahuel.dds.service;

public interface LocalFileDocumentService {
    /**
     * @param docType This key refers to the Deutsche
     * Bank internal document
     * category. Please note that this
     * key is known only to Deutsche
     * Bank and WebFG. When
     * interacting with 3rd party
     * document providers, we must
     * map this key to the providers
     *
     * @param instrumentId This key identifies the
     * instrument the document
     * belongs to. The key corresponds
     * to the instruments WKN which
     * is the German Nation
     * Identification number
     *
     * @param docLanguage This key identifies the language
     * the document should be in. DE
     * means German and En means
     * English.
     *
     * @return The document as a PDF file.
     */
    byte[] getDocument(String docType, String instrumentId, String docLanguage);
}
